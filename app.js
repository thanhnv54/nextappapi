const Express = require("express");
const BodyParser = require("body-parser");
const uuid = require('uuid/v1')


var app = Express();
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Data
var data = [
    {
        id: '1',
        name: 'John Brown',
        birthday: '05/24/1988',
        address: 'New York No. 1 Lake Park',
        phone: '096584233',
        prefix: '+84',
        email: 'test@mail.com',
        company: 'FPT'
    },
    {
        id: '2',
        name: 'Jim Green',
        birthday: '05/26/1988',
        address: 'London No. 1 Lake Park',
        phone: '055568552',
        prefix: '+84',
        email: 'thanh@mail.com',
        company: 'FPT'
    },
    {
        id: '3',
        name: 'Joe Black',
        birthday: '05/12/1988',
        address: 'Sidney No. 1 Lake Park',
        phone: '8622245223',
        prefix: '+84',
        email: 'tet@mail.com',
        company: 'FPT'
    },
    {
        id: '4',
        name: 'John Brown',
        birthday: '05/24/1988',
        address: 'New York No. 1 Lake Park',
        phone: '096584233',
        prefix: '+84',
        email: 'test@mail.com',
        company: 'FPT'
    },
    {
        id: '5',
        name: 'Jim Green',
        birthday: '05/26/1988',
        address: 'London No. 1 Lake Park',
        phone: '055568552',
        prefix: '+84',
        email: 'thanh@mail.com',
        company: 'FPT'
    },
    {
        id: '6',
        name: 'Joe Black',
        birthday: '05/12/1988',
        address: 'Sidney No. 1 Lake Park',
        phone: '8622245223',
        prefix: '+84',
        email: 'tet@mail.com',
        company: 'FPT'
    },
    {
        id: '7',
        name: 'John Brown',
        birthday: '05/24/1988',
        address: 'New York No. 1 Lake Park',
        phone: '096584233',
        prefix: '+84',
        email: 'test@mail.com',
        company: 'FPT'
    },
    {
        id: '8',
        name: 'Jim Green',
        birthday: '05/26/1988',
        address: 'London No. 1 Lake Park',
        phone: '055568552',
        prefix: '+84',
        email: 'thanh@mail.com',
        company: 'FPT'
    },
    {
        id: '9',
        name: 'Joe Black',
        birthday: '05/12/1988',
        address: 'Sidney No. 1 Lake Park',
        phone: '8622245223',
        prefix: '+84',
        email: 'tet@mail.com',
        company: 'FPT'
    },
    {
        id: '10',
        name: 'Jim Green',
        birthday: '05/26/1988',
        address: 'London No. 1 Lake Park',
        phone: '055568552',
        prefix: '+84',
        email: 'thanh@mail.com',
        company: 'FPT'
    },
    {
        id: '11',
        name: 'Joe Black',
        birthday: '05/12/1988',
        address: 'Sidney No. 1 Lake Park',
        phone: '8622245223',
        prefix: '+84',
        email: 'tet@mail.com',
        company: 'FPT'
    },
]

// Define REST API
app.post("/contact", async (request, response) => {
    try {
        let obj = await { id: uuid(), ...request.body }
        await data.unshift(obj);
        await response.status(200).send(data);
    } catch (error) {
        response.status(500).send(error);
    }
});


app.get("/contacts", async (request, response) => {
    response.send(data);
});
app.get("/contact/:id", async (request, response) => {
    try {
        let obj = await data.find((element) => {
            return element.id == request.params.id;
        })
        response.send(obj);
    } catch (error) {
        response.status(404).send(error);
    }

});
app.put("/contact/:id", async (request, response) => {
    try {
        let obj = await data.find((element) => {
            return element.id == request.params.id;
        })
        let ind = await data.indexOf(obj);
        await data.splice(ind, 1, request.body);
        response.send(data);
    } catch (error) {
        response.status(500).send(error);
    }
});
app.delete("/contact/:id", async (request, response) => {
    try {
        let obj = await data.find((element) => {
            return element.id == request.params.id;
        })
        let ind = await data.indexOf(obj);
        await data.splice(ind, 1);
        response.send(data);
    } catch (error) {
        response.status(404).send(error);
    }
});
app.listen(3000, () => {
    console.log("Listening at :3000...");
});

